package com.assignment1.constant;

public class DblpXmlTag {
	public static final String AUTHOR = "author";
	public static final String TITLE = "title";
	public static final String PAGES = "pages";
	public static final String YEAR = "year";
	public static final String VOLUME = "volume";
	public static final String JOURNAL = "journal";
	public static final String NUMBER = "number";
	public static final String EE = "ee";
	public static final String URL = "url";
	public static final String CROSSREF = "crossref";
	public static final String BOOKTITLE = "booktitle";
	public static final String ARTICLE = "article";
	public static final String INPROCEEDINGS = "inproceedings";
	
}
