package com.assignment1.error;

public class AssignmentError extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8896314225420856719L;
	
	public AssignmentError(String message) {
        super(message);
    }


}
