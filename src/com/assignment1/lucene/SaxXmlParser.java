package com.assignment1.lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.assignment1.constant.DblpXmlTag;
import com.assignment1.model.Article;
import com.assignment1.model.DblpRecord;
import com.assignment1.model.InProceedings;

public class SaxXmlParser extends DefaultHandler {
	private IndexWriter writer;
	private Stack<String> elementStack = new Stack<String>();
	private boolean isArticle = false;
	private boolean isInproceeding = false;
	private Article article;
	private InProceedings inproceedings;

	public SaxXmlParser(IndexWriter wr) {
		writer = wr;

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		this.elementStack.push(qName);
		if (qName.equalsIgnoreCase("article")) {
			article = new Article();
			isArticle = true;
		} else if (qName.equalsIgnoreCase("inproceedings")) {
			isInproceeding = true;
			inproceedings = new InProceedings();
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		this.elementStack.pop();
		try {
			if (qName.equalsIgnoreCase("article")) {
				System.out.println(article);
				Document doc = new Document();
				doc.add(new StringField("title", article.getTitle(),
						Field.Store.YES));
				writer.addDocument(doc);
				article = new Article();
				isArticle = false;
			} else if (qName.equalsIgnoreCase("inproceedings")) {
				Document doc = new Document();
				System.out.println(inproceedings);
				if (inproceedings.getTitle() != null) {
					doc.add(new StringField("title", inproceedings.getTitle(),
							Field.Store.YES));
				}
				if (inproceedings.getCrossref() != null) {
					doc.add(new StringField("Crossref", inproceedings
							.getCrossref(), Field.Store.YES));
				}
				writer.addDocument(doc);
				inproceedings = new InProceedings();
				isInproceeding = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println("End element"+qName);
	}

	@Override
	public void characters(char ch[], int start, int length)
			throws SAXException {
		String value = new String(ch, start, length).trim();
		if (value.length() == 0)
			return; // ignore white space
		if (DblpXmlTag.TITLE.equalsIgnoreCase(currentElement())) {
			if (isArticle) {
				article.setTitle(value);
			} else if (isInproceeding) {
				System.out.println("value is********Inproceeding(title) :"+value);
				inproceedings.setTitle(value);
			}
		} else if (DblpXmlTag.CROSSREF.equalsIgnoreCase(currentElement())) {
			if (isInproceeding) {
				System.out.println("value is********Inproceeding(crossref) :"+value);
				inproceedings.setCrossref(value);
			}
			
		} else if (DblpXmlTag.JOURNAL.equalsIgnoreCase(currentElement())) {
			article.setJournal(value);
		}

	}

	private String currentElement() {
		return this.elementStack.peek();
	}

}
