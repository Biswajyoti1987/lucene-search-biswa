package com.assignment1.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.assignment1.lucene.XmlFileIndexer;

public class AppMain {
	public static void main(String[] args) throws IOException {

		System.out.println("Welcome to Assignment 1");
		showMenu();
		XmlFileIndexer xIndexer = new XmlFileIndexer();
		int option = 0;
		while (option != 4) {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String s = br.readLine();
			option = Integer.parseInt(s);
			switch (option) {
			case 1:
				xIndexer.createIndex();
				break;
			case 2:
				xIndexer.reIndex();
				break;
			case 3:
			{
				System.out.println("Enter the search query: ");
				String query = br.readLine();
				System.out.println("Enter the index location: ");
				String idx = br.readLine();
				System.out.println("Enter the number of rows to return: ");
				s = br.readLine();
				int number = Integer.parseInt(s);
				xIndexer.doSearch(idx,number,query);
				break;
			}
			case 4:
				System.out.println("Thank you for using application");
				break;
			default:
				System.out.println("Invalid Option");
				break;

			}
			showMenu();
		}

	}

	/*
	 * Function to print the menu
	 */
	public static void showMenu() {
		System.out.println("***********DBLP Search Menu**********");
		System.out.println("[1]. Create Index");
		System.out.println("[2]. Re-index the file");
		System.out.println("[3]. Search");
		System.out.println("[4]. Exit");
		System.out.println("Option: ");
	}
}
