package com.assignment1.model;

public class DblpRecord {
	
	private String author;
	private String title;
	private String pages;
	private String year;
	private String url;
	private String ee;
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the pages
	 */
	public String getPages() {
		return pages;
	}
	/**
	 * @param pages the pages to set
	 */
	public void setPages(String pages) {
		this.pages = pages;
	}
	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the ee
	 */
	public String getEe() {
		return ee;
	}
	/**
	 * @param ee the ee to set
	 */
	public void setEe(String ee) {
		this.ee = ee;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DblpRecord [author=" + author + ", title=" + title + ", pages="
				+ pages + ", year=" + year + ", url=" + url + ", ee=" + ee
				+ "]";
	}
	
}
