package com.assignment1.model;

public class InProceedings extends DblpRecord {
	
	private String crossref;
	private String booktitle;
	/**
	 * @return the crossref
	 */
	public String getCrossref() {
		return crossref;
	}
	/**
	 * @param crossref the crossref to set
	 */
	public void setCrossref(String crossref) {
		this.crossref = crossref;
	}
	/**
	 * @return the booktitle
	 */
	public String getBooktitle() {
		return booktitle;
	}
	/**
	 * @param booktitle the booktitle to set
	 */
	public void setBooktitle(String booktitle) {
		this.booktitle = booktitle;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		System.out.print(super.toString());
		return "InProceedings [crossref=" + crossref + ", booktitle="
				+ booktitle + "]";
	}
	
}
